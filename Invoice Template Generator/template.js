/* Global Storage */
var current_selected_element;
var target_element;

function dragstart(event) {
    current_selected_element = event.target.id;
    console.log(current_selected_element);
}

function allowDrop(event) {
    event.preventDefault();
    target_element = event.target.id;
    console.log(target_element);
}

function drop(event) {
    event.preventDefault();
    if (current_selected_element === 'company_logo' && (target_element === "box1" || target_element === "box2" || target_element === "box3")) {
        var image_source = prompt('Enter the URL of the image', 'http://example.com');
        var image_url = image_source;
        var logo = document.createElement("img");
        logo.setAttribute("id", "logo");
        logo.setAttribute("src", image_url);
        logo.style.height = "100%";
        logo.style.width = "100%";
        document.getElementById(target_element).appendChild(logo);
    }

    if (current_selected_element === 'textbox' && (target_element === "box1" || target_element === "box2" || target_element === "box3")) {

        var input_field = document.createElement("input");
        input_field.style.width = "98%";
        input_field.style.height = "20%";
        input_field.style.border = "0";
        document.getElementById(target_element).appendChild(input_field);

    }
}


/* Handler for table drag and drop events. */

var table_id;
var table_spot_id;
var no_of_cols;
var rowCounter = 1;

function tabledrag(event) {
    console.log("tabledrag");
    table_id = event.target.id;
}

function allowTableDrop(event) {
    console.log("allowtabledrop");
    event.preventDefault();
    table_spot_id = event.target.id;
}

function tableDrop(event) {
    var prompt_col = prompt("Enter the number of columns", "Example: Min: 1 Max:8");
    var columns = prompt_col;
    no_of_cols = columns;
    if (columns <= 8 && columns >= 1) {

		
		/* The Code Handling the header of the inserted table */
	
        var div_containing_header_divs = document.createElement("div");
		div_containing_header_divs.setAttribute("class","table_blocks");

        for (var i = 0; i < columns; i++) {
            var header_divs = document.createElement("div");
            header_divs.setAttribute("class", "table_header_cells");
            div_containing_header_divs.appendChild(header_divs);
        }
		
		/*---------------------------------------------------------*/
      
		
		/* The code generating the first row for the table */
		
		
        var div_containing_table_cell_divs = document.createElement("div");
		div_containing_table_cell_divs.setAttribute("class","table_blocks");
        for (var i = 0; i < columns; i++) {
            var first_row_cell = document.createElement("div");
            first_row_cell.setAttribute("class", "table_row_cells");
            div_containing_table_cell_divs.appendChild(first_row_cell);
        }

		/*---------------------------------------------------------*/
		
		
        var add_row_button = document.createElement("div");
        add_row_button.setAttribute("id", "add_row_button");
        add_row_button.setAttribute("onclick", "addRow(event)");
        var text_node = document.createTextNode("+ Add Row");
        add_row_button.appendChild(text_node);

        document.getElementById("section3").appendChild(div_containing_header_divs);
        document.getElementById("section3").appendChild(div_containing_table_cell_divs);
        document.getElementById("section3").appendChild(add_row_button);

    } else {
        alert("Please enter a number between 1 to 8");
    }

}


function addRow(event) {
    rowCounter++;
    var nextRow = document.createElement("div");
	nextRow.setAttribute("class","table_blocks");

    for (var i = 0; i < no_of_cols; i++) {
        var new_row_element = document.createElement("div");
        new_row_element.setAttribute("class", "table_row_cells");
        nextRow.appendChild(new_row_element);
    }

    document.getElementById("section3").insertBefore(nextRow, document.getElementById(event.target.id));
}