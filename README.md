# Dynamic Invoice Template Generator #
## HTML, CSS, JavaScript ##

### Web Application that enables a user to create customized invoices by allowing the user to select a specific set of fields and images that he can drag and drop onto a template and then the template is extracted and converted to a PDF with the help of a Flask Framework. ###